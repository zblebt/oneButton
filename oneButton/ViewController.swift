//
//  ViewController.swift
//  oneButton
//
//  Created by Jan Bednar on 05/09/16.
//  Copyright © 2016 Jan Bednar. All rights reserved.
//

import UIKit
import SnapKit

class ViewController: UIViewController {

    
    internal var button: UIButton = UIButton()
    internal var label = UILabel()
    internal var number = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .whiteColor()
        view.addSubview(button)
        
        button.snp_makeConstraints { (make) in
            make.center.equalTo(self.view)
        }
        
        button.setTitle("Click me", forState: .Normal)
        button.setTitleColor(.blackColor(), forState: .Normal)
        button.addTarget(self, action: #selector(ViewController.clicked), forControlEvents: .TouchUpInside)
        
        button.isAccessibilityElement = true
        button.accessibilityIdentifier = "my_button"
        //button.accessibilityLabel = "my_label_button"
        //button.accessibilityValue = "value_button"
        
        view.addSubview(label)
        
        label.snp_makeConstraints { (make) in
            make.centerX.equalTo(button)
            make.bottom.equalTo(button.snp_top).offset(-30)
        }
        
        label.text = "\(number)"
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @objc private func clicked() {
        number += 1
        label.text = "\(number)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

